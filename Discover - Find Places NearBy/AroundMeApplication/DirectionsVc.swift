//
//  DirectionsVc.swift
//  AroundMeApplication
//
//  Created by Admin on 5/24/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker
import Alamofire

class DirectionsVc: UIViewController, CLLocationManagerDelegate {
    
    var mapView: GMSMapView!
    
    var destination : CLLocationCoordinate2D?
    var source : CLLocationCoordinate2D?
    
    var _destination : CLLocationCoordinate2D?
    {
        get{ return destination }
        set{ _destination = destination }
    }
    var _source : CLLocationCoordinate2D?
    {
        get{ return source }
        set{ _source = source }
    }
    
    
    func loadGMS() {
        
        let current_Latitude =  MapView.latt
        let current_Longitude = MapView.long
        
        let cameraPosition = GMSCameraPosition.camera(withLatitude: current_Latitude!, longitude: current_Longitude!, zoom: 16.0)
        
        self.mapView = GMSMapView.map(withFrame: CGRect.zero, camera: cameraPosition)
        self.mapView.isMyLocationEnabled = true
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: current_Latitude!, longitude: current_Longitude!)        // marker.icon = UIImage(named: "aaa.png")!
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
        marker.map = mapView
        let path = GMSMutablePath()
        self.view = mapView
        
    }
    
    
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "http://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=false&mode=driving")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        let routes = json["routes"] as? [Any]
                        print(routes)
                        
                        
                        OperationQueue.main.addOperation({
                            for route in routes!
                            {
                                let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                                let points = routeOverviewPolyline.object(forKey: "points")
                                let path = GMSPath.init(fromEncodedPath: points! as! String)
                                let polyline = GMSPolyline.init(path: path)
                                polyline.strokeWidth = 5.0
                                polyline.strokeColor = UIColor.blue
                                
                                let bounds = GMSCoordinateBounds(path: path!)
                                print("b= \(bounds)")
                                
                                polyline.map = self.mapView
                            }
                        })
                        
                    }
                }
                catch{
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
    
    func showPath(polyStr :String){
        let path = GMSPath(fromEncodedPath: polyStr)
        let polyline = GMSPolyline(path: path)
        polyline.strokeWidth = 3.0
        polyline.map = mapView // Your map view
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadGMS()
        getPolylineRoute(from: _source!, to: _destination!)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

