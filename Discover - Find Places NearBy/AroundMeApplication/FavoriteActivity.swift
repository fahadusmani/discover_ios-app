//
//  FavoriteActivity.swift
//  AroundMeApplication
//
//  Created by Admin on 5/12/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class FavoriteActivity: UIActivity {
     func activityType() -> String? {
        return "TestActionss.Favorite"
    }
    
    override func activityTitle() -> String? {
        return "Add to Favorites"
    }
    
    override func canPerformWithActivityItems(activityItems: [AnyObject]) -> Bool {
        NSLog("%@", #function)
        return true
    }
    
    override func prepareWithActivityItems(activityItems: [AnyObject]) {
        NSLog("%@", __FUNCTION__)
    }
    
    override func activityViewController() -> UIViewController? {
        NSLog("%@", __FUNCTION__)
        return nil
    }
    
    override func performActivity() {
        // Todo: handle action:
        NSLog("%@", __FUNCTION__)
        
        self.activityDidFinish(true)
    }
    
    override func activityImage() -> UIImage? {
        return UIImage(named: "favorites_action")
    }
}
