//
//  ViewController.swift
//  PlacePicker
//
//  Created by Malek T. on 10/26/15.
//  Copyright © 2015 Medigarage Studios LTD. All rights reserved.
//
/*
protocol PlacesRequestDelegate: class {
    func sendPlacesData(data : [placeData])
}


import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker



class LoadGms: UIViewController, CLLocationManagerDelegate {
    
    var p = [placeData]()
    weak var delegate : PlacesRequestDelegate?
    @IBOutlet var mapViewContainer: UIView!
    
    var googleMapView: GMSMapView!
    
    var locationManager: CLLocationManager!
    var placePicker: GMSPlacePicker!
    var latitude: Double!
    var longitude: Double!
    
    static var website : String?
    static var phone : String?
    
    var likelyPlaces: [GMSPlace] = []
    
    var selectedPlace: GMSPlace?
    
    var placesClient : GMSPlacesClient?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.startUpdatingLocation()
        
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        
        }
    
    func loadPlaces()
    {
        placesClient = GMSPlacesClient.shared()
        
        let placeID = "ChIJV4k8_9UodTERU5KXbkYpSYs"
        
        placesClient?.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeID)")
                return
            }
            
            print("Place name \(place.name)")
            print("Place number \(place.phoneNumber)")
            print("Place web \(place.website)")
            print("Place timing \(place.openNowStatus)")
            LoadGms.phone = place.phoneNumber!
            LoadGms.website = String(describing: place.website!)
        })
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func place()
    {
        var data = placeData()
        placesClient = GMSPlacesClient.shared()
        
        let placeID = "ChIJV4k8_9UodTERU5KXbkYpSYs"
        
        placesClient?.lookUpPlaceID(placeID, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeID)")
                return
            }
            
            print("Place name \(place.name)")
            print("Place number \(place.phoneNumber)")
            print("Place web \(place.website)")
            print("Place timing \(place.openNowStatus)")
            LoadGms.phone = place.phoneNumber!
            LoadGms.website = String(describing: place.website!)
            
            data.phone = place.phoneNumber
           
            data.website = String(describing: place.website)
            
            self.p.append(data)
        })
        self.delegate?.sendPlacesData(data: p)
    }
    
    
    
  
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager,
                         didUpdateLocations locations: [CLLocation]){
        // 1
        let location:CLLocation = locations.last!
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        
        // 2
        let coordinates = CLLocationCoordinate2DMake(self.latitude, self.longitude)
        
    }
    
    
    
    
    func locationManager(_ manager: CLLocationManager,
                         didFailWithError error: Error){
        
        print("An error occurred while tracking location changes : \(error.localizedDescription)")
    }
    
    
    
    
    
    
    
    
    
}

*/




