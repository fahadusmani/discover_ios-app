//
//  SettingVc.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//
import UIKit
import GoogleMobileAds

class SettingVc: UIViewController, UITableViewDataSource, UITableViewDelegate, ColorCellDelegate, GADBannerViewDelegate
{
    var cellController = SettingCellController()
    var googleAds = GoogleAdsServices()
    static var themeColor : UIColor?
    var objColor = colorSelect()
    var cellDataObj = detailCellController()
    var imageObject = ImageColor()
    static var selected = 0
    static var radiusType = 0.0
    var radiusSymbol = ""
    var div = 1.0
    var radiusMoving = false
    
    @IBOutlet weak var settingAdsView: GADBannerView!
    @IBOutlet weak var settingTable: UITableView!
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 6)
        {
            return CGFloat(90)
        }
        else
        {
            return CGFloat(40)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        if (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 4)
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
            cell = cellController.getSettingsTableData(cell: cell, row: indexPath.row)
            
            return cell
        }
            
        else if(indexPath.row == 1)
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "colorCell", for: indexPath) as! ColorCellButtons
            cell.colorDelegate = self
            cell = cellController.applyCellAttributes(cell: cell, index: 1)
            
            return cell
        }
            
        else if(indexPath.row == 3)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "distanceCell", for: indexPath) as! ColorCellButtons
            cell.colorDelegate = self
            cellController.applyCellAttributes(cell: cell, index: 3)
            
            if(ListVc.distanceUnitKm)
            {
                cell.segment.selectedSegmentIndex = 0
            }
            else
            {
                cell.segment.selectedSegmentIndex = 1
            }
            
            return cell
        }
            
        else if(indexPath.row == 5)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "radiusCell", for: indexPath) as! ColorCellButtons
            cell.colorDelegate = self
            
            cell.radiusSegmentLabel.text = "Current Radius"
            
            if (radiusSymbol == "km")
            {
                div = 1
            }
            else{
                div = 1.609
            }
            cell.radiusValueLabel.text = "\((SettingVc.radiusType/div).roundTo(places: 0)) \(radiusSymbol)"
            
            cell.radiusSegmentLabel.font = colorSelect.textFont
            cell.radiusValueLabel.font = colorSelect.textFont
            cell.backgroundColor = colorSelect.settingsTitleColor
            cell.selectionStyle = .none
            return cell
        }
            
        else if(indexPath.row == 6)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "radiusSlider", for: indexPath) as! ColorCellButtons
            cell.colorDelegate = self
            
            if (radiusSymbol == "km")
            {
                div = 1
                cell.maxRadiusLabel.text = "0 Km"
                cell.minRadiusLabel.text = "50 Km"
            }
            else{
                div = 1.609
                cell.maxRadiusLabel.text = "0 Miles"
                cell.minRadiusLabel.text = "31 Miles"
            }
            cell.silderThumbLabel.text = "\((SettingVc.radiusType/div).roundTo(places: 0))"
            
            cell.maxRadiusLabel.font = colorSelect.textFont
            cell.minRadiusLabel.font = colorSelect.textFont
            cell.radiusSliderLabel.tintColor = colorSelect.color
            
            cell.radiusSliderLabel.setValue(Float(JsonRequest.distance), animated: true)
            
            var totalWidth = cell.radiusSliderLabel.frame.width
            totalWidth =  totalWidth - 30
            
            var newPos =   Double(totalWidth) *  (SettingVc.radiusType/50)
            
            let x = cell.radiusSliderLabel.frame.origin.x
            cell.silderThumbLabel.frame = CGRect(x: newPos, y: 7, width: 40, height: 20)
            
            
            
            cell.backgroundColor = colorSelect.settingsTitleColor
            
            cell.silderThumbLabel.isHidden = false
            
            cell.silderThumbLabel.backgroundColor = UIColor.white
            
            cell.silderThumbLabel.textColor = colorSelect.color
            
            cell.silderThumbLabel.layer.borderWidth = 1.0
            cell.silderThumbLabel.layer.borderColor = colorSelect.color?.cgColor
            cell.silderThumbLabel.font =  UIFont(name: "Noto Sans", size: 12.0)//colorSelect.textFont
            cell.silderThumbLabel.layer.masksToBounds = true
            cell.silderThumbLabel.layer.cornerRadius = 5.0
            
            cell.selectionStyle = .none
            
            return cell
        }
            
        else if(indexPath.row == 7)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GridCell", for: indexPath) as! ColorCellButtons
            cell.colorDelegate = self
            
            cell.backgroundColor = colorSelect.settingsTitleColor
            
            cell.viewSegment.tintColor = colorSelect.color
            
            cell.viewSegmentLabel.font = colorSelect.titleFont
            cell.viewSegmentLabel.textColor = colorSelect.color
            
            cell.selectionStyle = .none
            if HomeContainerView.i == 0
            {
                cell.viewSegment.selectedSegmentIndex = 0
            }
            else
            {
                cell.viewSegment.selectedSegmentIndex = 1
            }
            return cell
        }
            
        else
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "cell2",for: indexPath)
            cell = cellDataObj.getSettingsTableData(cell: cell, row: indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setRadiusUnit()
        objColor.setNavigationTitleImage(item: self.navigationItem)
        googleAds.loadAds(myBanner: settingAdsView)
        settingAdsView.rootViewController = self
        settingTable.tableFooterView = UIView(frame: .zero)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.tabBarController?.tabBar.isHidden = false
        objColor.fetchColor()           //applying theme
        objColor.applyColor(nav: self.navigationController!, bar: self.tabBarController!)
        settingTable.reloadData()
    }
    
    func setRadiusUnit()
    {
        if(ListVc.distanceUnitKm)
        {
            radiusSymbol = "km"
        }
        else
        {
            radiusSymbol = "miles"
        }
    }
    
    
    func segmentedIndex(cell: ColorCellButtons)
    {
        switch cell.segment.selectedSegmentIndex   //setting distance units
        {
        case 0:
            ListVc.distanceUnitKm = true
            radiusSymbol = "km"
            objColor.saveDistanceSettings(disInKm: true)
            
        case 1:
            ListVc.distanceUnitKm = false
            objColor.saveDistanceSettings(disInKm: false)
            radiusSymbol = "mile"
            
        default:
            break
        }
        JsonData.updateDistanceValue = 1
        ListVc.distanceUpdate = 1
        settingTable.reloadData()
    }
    
    func segmentedIndexforRadius(cell: ColorCellButtons)
    {
    }
    
    func radiusSliderValue(cell: ColorCellButtons)
    {
        radiusMoving = true
        JsonRequest.distance = Int(cell.radiusSliderLabel.value)
        SettingVc.radiusType = Double(cell.radiusSliderLabel.value/1000)
        
        JsonData.updateDistanceValue = 1
        ListVc.distanceUpdate = 1
        
        
        var totalWidth = cell.radiusSliderLabel.frame.width//settingTable.frame.width
        totalWidth =  totalWidth - 30
        
        var newPos =   Double(totalWidth) *  (SettingVc.radiusType/50)
        
        
        let x = cell.radiusSliderLabel.frame.origin.x
        cell.silderThumbLabel.frame = CGRect(x: newPos, y: 7, width: 40, height: 20)
        
        objColor.saveDistanceValue(distance: Int(cell.radiusSliderLabel.value), radiusValue: SettingVc.radiusType)
        
        settingTable.reloadData()
    }
    
    
    func viewSegment(cell: ColorCellButtons)
    {
        switch cell.viewSegment.selectedSegmentIndex
        {
        case 0:
            HomeContainerView.i = 0
            objColor.saveListSetting(isListEnabled: 0)
            
        case 1:
            HomeContainerView.i = 1
            objColor.saveListSetting(isListEnabled: 1)
        default:
            break
        }
    }
    
    
    func customSlider2(cell: ColorCellButtons)
    {
    }
    
    func radiusPickerView(cell: ColorCellButtons)
    {
    }
    
    func blueTapped(cell: ColorCellButtons)
    {
        objColor.saveColor(r: 27,g: 154,b: 247)
        SettingVc.selected = 2
        viewWillAppear(true)
    }
    
    func purpleTapped(cell: ColorCellButtons)
    {
        objColor.saveColor(r: 103,g: 58,b: 183)
        SettingVc.selected = 1
        viewWillAppear(true)
    }
    
    func redTapped(cell: ColorCellButtons)
    {
        objColor.saveColor(r: 250,g: 67,b: 81)
        SettingVc.selected = 3
        viewWillAppear(true)
    }
    func greenTapped(cell: ColorCellButtons)
    {
        objColor.saveColor(r: 0,g: 151,b: 167)
        SettingVc.selected = 4
        viewWillAppear(true)
    }
    
}
