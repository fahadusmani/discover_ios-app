//
//  DetailVc.swift
//  AroundMeApplication
//
//  Created by Admin on 4/20/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import CoreData
import MapKit
import CoreLocation
import GoogleMaps
import GooglePlaces
import GoogleMapsCore
import GooglePlacePicker
import NVActivityIndicatorView

class DetailVc: UIViewController, UITableViewDelegate, UITableViewDataSource, ButtonCellDelegate,JsonRequestDelegate,LoadMapDelegate
{
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var detailTable: UITableView!
    //var activityViewController : UIActivityViewController?
    var activityIndicatorView1 : NVActivityIndicatorView!
    
    static var img : UIImage?
    static var logoImages = [UIImage]()
    var imageObj = ImageColor()
    var alertObj = AlertViewws()
    var callingServiceObj = CallingService()
    var setDataObj = SetData()
    var appMapObj = AppleMap()
    var _receivedPlaceData = [ListDataArray]()
    var jsonReqObj = JsonRequest()
    var coreDataObj = CoreDataModel()
    var favObj = FavoriteVc()
    var saveDataObj = SaveFavourite()
    var mapObj = MapView()
    var loadMapObj = LoadMap()
    var cellDataObject = detailCellController()
    var objColor = colorSelect()
    static var isFavorite : Bool?
    var loaderObject = Loader()
    
    
    var i = 0
    var dataArray = [AnyObject]()
    var favData = [DetailDataArray]()
    var placesData = [placeData]()
    var placeShedule = [String]()
    var getDataArray = [DetailDataArray]()
    var placePhotos = [UIImage]()
    var _getDataArray : [DetailDataArray]?
    {
        didSet{
            getDataArray = _getDataArray!
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        var x = 0
        if(indexPath.row == 0 || indexPath.row == 2)
        {
            x = 60
        }
            
        else if (indexPath.row == 5)
        {
            if(_receivedPlaceData.count == 0 || _receivedPlaceData[0].website! == "")
            {
                x = 0
            }
            else
            {
                x = 30
            }
        }
            
        else if(indexPath.row == 6)
        {
            if(_receivedPlaceData.count == 0 || _receivedPlaceData[0].phone! == "")
            {
                x = 0
            }
            else
            {
                x = 30
            }
        }
            
        else if(indexPath.row == 11 || indexPath.row == 12 || indexPath.row == 13 || indexPath.row == 14 || indexPath.row == 15 || indexPath.row == 16 || indexPath.row == 17)
        {
            if( _receivedPlaceData.count != 0 && _receivedPlaceData[0].shedule! != [])
            {
                x = 25
            }
        }
            
        else if(indexPath.row == 19)
        {
            if(DetailVc.logoImages == [])
            {
                x = 0
            }
            else
            {
                x = 130
            }
        }
            
        else
        {
            x = 40
        }
        return CGFloat(x)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "btnCell", for: indexPath) as! ButtonCell    //cell for buttons
            cell.buttonDelegate = self
            let newCell = cellDataObject.getButtonCellData(cell: cell) as! ButtonCell
            cell.selectionStyle = .none
            return newCell
        }
        
        if(indexPath.row == 19)
        {   //cell for photos
            let cell = tableView.dequeueReusableCell(withIdentifier: "contact", for: indexPath) as! TableViewCelll
            
            if placePhotos.count < 0
            {
                cell.isHidden = true
            }
            cell.selectionStyle = .none
            return cell
        }
            
        else
        {
            var cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath)
            cell = cellDataObject.setTitleFonts(cell: cell)
            
            switch indexPath.row
            {
            case 1:
                cell.textLabel?.text = "Address"
                cell.imageView?.image = nil
                break
                
            case 3:
                cell.textLabel?.text = "Contact"
                cell.imageView?.image = nil
                break
                
            case 7:
                cell.textLabel?.text = "Distance"
                cell.imageView?.image = nil
                break
                
            case 18:
                
                if(placePhotos == [])
                {
                    cell.backgroundColor = UIColor.white
                    cell.isHidden = true
                }
                else
                {
                    cell.textLabel?.text = "Photos"
                    cell.imageView?.image = nil
                }
                break
                
            case 9:
                cell.textLabel?.text = "Opening Hours"
                cell.imageView?.image = nil
                break
                
            case 10:
                cellDataObject.getCellData(cell: cell, index: 10)
                break
            case 11:
                cellDataObject.getCellData(cell: cell, index: 11)
                break
            case 12:
                cellDataObject.getCellData(cell: cell, index: 12)
                break
            case 13:
                cellDataObject.getCellData(cell: cell, index: 13)
                break
            case 14:
                cellDataObject.getCellData(cell: cell, index: 14)
                break
            case 15:
                cellDataObject.getCellData(cell: cell, index: 15)
                break
            case 16:
                cellDataObject.getCellData(cell: cell, index: 16)
                break
            case 17:
                cellDataObject.getCellData(cell: cell, index: 17)
                break
                
            case 2:
                cell = cellDataObject.getCellData(cell: cell, index: 2)
                break
                
            case 4:
                cell = cellDataObject.getCellData(cell: cell, index: 4)
                break
                
            case 5:
                cell = cellDataObject.getCellData(cell: cell, index: 5)
                break
                
            case 6:
                cell = cellDataObject.getCellData(cell: cell, index: 6)
                break
                
            case 8:
                cell = cellDataObject.getCellData(cell: cell, index: 8)
                break
                
            default:
                break
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    //functions for embedding collection view in tableView for Photos
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        guard let tableViewCell = cell as? TableViewCelll else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    }
    
    internal func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath)
    {
        guard let tableViewCell = cell as? TableViewCelll else
        {
            return
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 4
        {
            let alert = alertObj.setMapAlert(placeName: getDataArray[0].name!, latitude: getDataArray[0].latitude!, Longitude: getDataArray[0].longitude!)
            present(alert, animated: true, completion: nil)
        }
        
        if indexPath.row == 5
        {
            callingServiceObj.openWebsite(website: _receivedPlaceData[0].website!)
        }
        
        if indexPath.row == 6
        {
            callingServiceObj.makeCall(phone: _receivedPlaceData[0].phone!)
        }
    }
    
    func favTapped(cell: ButtonCell)
    {
        if(DetailVc.isFavorite == false)
        {
            let data = setDataObj.favoriteData(array: getDataArray, array1: _receivedPlaceData)
            favData.append(data)
            saveDataObj.save(data: favData)
            viewWillAppear(false)
        }
        else
        {
            coreDataObj.deleteData(name: getDataArray[0].name!)
            viewWillAppear(false)
        }
    }
    
    func currenTTapped(cell: ButtonCell)
    {
        MapView.des = CLLocationCoordinate2DMake(getDataArray[0].latitude!, getDataArray[0].longitude!)
        MapView.src = CLLocationCoordinate2DMake(LoadMap.latitude!, LoadMap.longitude!)
        MapView.showDirection = 1
        var showMap : UIViewController = self.childViewControllers[0] as! UIViewController
        showMap.viewDidLoad()
    }
    
    
    func sharedTapped(cell: ButtonCell)
    {
        let activityViewController = alertObj.setShareWidget(array: getDataArray, array1: _receivedPlaceData)
        present(activityViewController, animated: true, completion: {})
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        objColor.setNavigationTitleImage(item: self.navigationItem)
        activityIndicatorView1 = loaderObject.showActivityIndicatory1(uiView: self.view)
        activityIndicatorView1.startAnimating()
        detailTable.isHidden = true
        self.view.backgroundColor = colorSelect.settingsTitleColor
        loadMapObj.mapDelegate = self
        loadMapObj.loadPhotos(pid: getDataArray[0].placeId!)
        jsonReqObj.delegate = self
        jsonReqObj.updateShedule(pid: getDataArray[0].placeId!)
        cellDataObject.array = getDataArray
        detailTable.tableFooterView = UIView(frame: .zero)
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.tabBarController?.tabBar.isHidden = true
        objColor.fetchColor()           //applying theme
        objColor.applyColor(nav: self.navigationController!, bar: self.tabBarController!)
        detailTable.reloadData()
    }
    
    func sendPlacesData(data: [placeData])
    {
        DispatchQueue.main.async
            {
                self.placesData = data
                self.detailTable.reloadData()
        }
    }
    var x = 0
    func sendPhotosData(data: [UIImage])
    {
        DispatchQueue.main.async
            {
                
                self.activityIndicatorView1.stopAnimating()
                self.view.backgroundColor = UIColor.white
                self.detailTable.isHidden = false
                self.placePhotos = data
        }
    }
    
    func sendData(data: [Info])
    {}
    
    func sendData1(data: [ListDataArray])
    {
        DispatchQueue.main.async
            {
                self._receivedPlaceData = data
                self.detailTable.reloadData()
                self.cellDataObject.array1 = self._receivedPlaceData
        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if (segue.identifier == "toMapView")
        {
            let dest = segue.destination as! MapView
            MapView.showDirection = 0
            MapView.latt = getDataArray[0].latitude!
            MapView.long = getDataArray[0].longitude!
            MapView.placeName = getDataArray[0].name
            MapView.placeAddress = getDataArray[0].location
        }
        if(segue.identifier == "toDirectionsVc")
        {
            let dest = CLLocationCoordinate2DMake(getDataArray[0].latitude!, getDataArray[0].longitude!)
            let sourcee = CLLocationCoordinate2DMake(LoadMap.latitude!, LoadMap.longitude!)
            
            let directionViewController = segue.destination
                as! DirectionsVc
            
            directionViewController.source = sourcee
            directionViewController.destination = dest
            
        }
    }
}


extension DetailVc : UICollectionViewDelegate, UICollectionViewDataSource
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        
        if(placePhotos.count > 0)
        {
            return placePhotos.count
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        if(placePhotos.count > 0)
        {
            cell.collectionImageView.image = placePhotos[indexPath.row]
        }
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        {
            layout.scrollDirection = .horizontal
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let imagee = placePhotos[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CollectionViewCell
        
        let imageView = cell.collectionImageView.image
        let newImageView = UIImageView(image: imagee)
        newImageView.frame = UIScreen.main.bounds
        newImageView.backgroundColor = .black
        newImageView.contentMode = .scaleAspectFit
        newImageView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
        newImageView.addGestureRecognizer(tap)
        self.view.addSubview(newImageView)
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func dismissFullscreenImage(sender: UITapGestureRecognizer)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.tabBarController?.tabBar.isHidden = true
        sender.view?.removeFromSuperview()
        
        let indexPath =
            NSIndexPath(row: 19, section: 0)
        detailTable.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
        
    }
}
