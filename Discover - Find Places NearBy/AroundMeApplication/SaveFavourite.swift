//
//  SaveFavourite.swift
//  AroundMeApplication
//
//  Created by Admin on 4/25/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SaveFavourite
{
    func save(data : [DetailDataArray])
    {
        guard let appDelegate =
            UIApplication.shared.delegate as? AppDelegate else {
                return
        }
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        let entity =
            NSEntityDescription.entity(forEntityName: "Favorite",
                                       in: managedContext)!
        
        let entityData = NSManagedObject(entity: entity,
                                         insertInto: managedContext)
        
        entityData.setValue(data[data.count-1].name, forKeyPath: "name")
        entityData.setValue(data[data.count-1].latitude, forKeyPath: "latitude")
        entityData.setValue(data[data.count-1].longitude, forKeyPath: "longitude")
        entityData.setValue(data[data.count-1].location, forKeyPath: "location")
        entityData.setValue(data[data.count-1].website, forKeyPath: "website")
        entityData.setValue(data[data.count-1].phone, forKeyPath: "phone")
        entityData.setValue(data[data.count-1].openNow, forKeyPath: "openNow")
        entityData.setValue(data[data.count-1].placeId, forKeyPath: "placeID")
        entityData.setValue(data[data.count-1].distance, forKeyPath: "distance")
        entityData.setValue(data[data.count-1].timings, forKeyPath: "timings")
        
        do
        {
            try managedContext.save()
            CoreDataModel.people.append(entityData)
        }
            
        catch let error as NSError
        {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    
}
