//
//  MapView.swift
//  AroundMeApplication
//
//  Created by Admin on 4/20/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//
import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class MapView: UIViewController {
    
    @IBOutlet weak var textLabel: UILabel!
    
    static var latt : Double?
    static var long : Double?
    static var placeName : String?
    static var placeAddress : String?
    static var showDirection : Int?
    static var src : CLLocationCoordinate2D?
    static var des : CLLocationCoordinate2D?
    
    var googleMapView: GMSMapView!
    var mapView : GMSMapView!
    var strokeNewColor : CGColor?
    var imageObject = ImageColor()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        loadMap()
        if MapView.showDirection == 1
        {
            getPolylineRoute(from: MapView.src!, to: MapView.des!)
        }
        view.layer.cornerRadius = 15.0
        view.layer.borderWidth = 2.0
        view.layer.borderColor = colorSelect.color?.cgColor
        setMarker()
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    func setMarker()
    {
        let current_Latitude =  MapView.latt
        let current_Longitude = MapView.long
        let destImage = imageObject.maskWithColor(img: #imageLiteral(resourceName: "map_destination"), color: colorSelect.color!)
        let sourceImage = imageObject.maskWithColor(img: #imageLiteral(resourceName: "map_currentLocation"), color: colorSelect.color!)
        
        let destinationImageView = UIImageView(image: destImage)
        
        let homeImageView = UIImageView(image: sourceImage)
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: current_Latitude!, longitude: current_Longitude!)
        
        marker.iconView = destinationImageView
        marker.tracksViewChanges = true
        
        marker.title = MapView.placeName
        marker.snippet = MapView.placeAddress
        marker.map = mapView
        
        let marker1 = GMSMarker()
        marker1.position = CLLocationCoordinate2D(latitude: LoadMap.latitude!, longitude: LoadMap.longitude!)
        
        marker1.iconView = homeImageView
        marker1.title = "Current Location"
        marker1.tracksViewChanges = true
        marker1.map = mapView
    }
    
    func loadMap()
    {
        let current_Latitude =  MapView.latt
        let current_Longitude = MapView.long
        var locationManager: CLLocationManager!
        
        let camera = GMSCameraPosition.camera(withLatitude: current_Latitude!, longitude: current_Longitude!, zoom: 18.0)
        
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        
        view = mapView
        
    }
    
    func getPolylineRoute(from source: CLLocationCoordinate2D, to destination: CLLocationCoordinate2D){
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let url = URL(string: "http://maps.googleapis.com/maps/api/directions/json?origin=\(source.latitude),\(source.longitude)&destination=\(destination.latitude),\(destination.longitude)&sensor=true&mode=driving")!
        
        let task = session.dataTask(with: url, completionHandler: {
            (data, response, error) in
            if error != nil {
                print(error!.localizedDescription)
            }else{
                do {
                    if let json : [String:Any] = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String: Any]{
                        
                        let routes = json["routes"] as? [Any]
                        
                        OperationQueue.main.addOperation({
                            for route in routes!
                            {
                                let routeOverviewPolyline:NSDictionary = (route as! NSDictionary).value(forKey: "overview_polyline") as! NSDictionary
                                let points = routeOverviewPolyline.object(forKey: "points")
                                let path = GMSPath.init(fromEncodedPath: points! as! String)
                                let polyline = GMSPolyline.init(path: path)
                                polyline.strokeWidth = 5.0
                                polyline.strokeColor = colorSelect.color!
                                
                                let bounds = GMSCoordinateBounds(path: path!)
                                self.mapView.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                                polyline.map = self.mapView
                            }
                        })
                    }
                }
                catch{
                    print("error in JSONSerialization")
                }
            }
        })
        task.resume()
    }
}
