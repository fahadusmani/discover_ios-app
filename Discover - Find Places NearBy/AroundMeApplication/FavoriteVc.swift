//
//  FavoriteVc.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import CoreData
import GoogleMobileAds

class FavoriteVc: UIViewController,UITableViewDelegate, UITableViewDataSource, GADBannerViewDelegate {
    
    var coreDataObj = CoreDataModel()
    var favData = [DetailDataArray]()
    var dataToSend = [DetailDataArray]()
    var imageObj = ImageColor()
    var setDataObj = SetData()
    var objColor = colorSelect()
    var cellController = FavoriteCellController()
    var googleAds = GoogleAdsServices()
    
    @IBOutlet weak var favTable: UITableView!
    @IBOutlet weak var favoriteAdsView: GADBannerView!
    @IBOutlet weak var viewImageLabel: UIImageView!
    @IBOutlet weak var viewTextLabel: UILabel!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return favData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        viewImageLabel.isHidden = true
        viewTextLabel.isHidden = true
        let cellIdentifier = "cell"
        var cell : UITableViewCell  = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as UITableViewCell
        cell = cellController.applyCellAttribures(cell: cell, array: favData, index: indexPath.row)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        dataToSend = cellController.favToDetail(array: favData, row: indexPath.row)
        performSegue(withIdentifier: "toDetailVc", sender: self)
    }
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        objColor.setNavigationTitleImage(item: self.navigationItem)
        googleAds.loadAds(myBanner: favoriteAdsView)   //loading ADS
        favoriteAdsView.rootViewController = self
        favTable.tableFooterView = UIView(frame: .zero)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        favTable.isHidden = false
        objColor.fetchColor()           //applying theme
        objColor.applyColor(nav: self.navigationController!, bar: self.tabBarController!)
        favData = coreDataObj.fetchData()
        
        if(favData.count < 1)
        {
            displayMessage()
        }
        favTable.reloadData()
    }
    
    func displayMessage()
    {
        favTable.isHidden = true
        viewTextLabel.isHidden = false
        viewImageLabel.isHidden = false
        viewImageLabel.image = #imageLiteral(resourceName: "fav_noFavorite")
        viewTextLabel.text = "Star Your Favorite Location"
        viewTextLabel.font = colorSelect.titleFont
        viewTextLabel.textColor = UIColor.lightGray
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        let dest = segue.destination as! DetailVc
        dest._getDataArray = dataToSend
    }
    
}
