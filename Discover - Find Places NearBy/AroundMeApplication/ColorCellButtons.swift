//
//  ColorCellButtons.swift
//  AroundMeApplication
//
//  Created by Admin on 4/28/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

protocol ColorCellDelegate
{
    func blueTapped(cell: ColorCellButtons)
    func redTapped(cell: ColorCellButtons)
    func greenTapped(cell: ColorCellButtons)
    func purpleTapped(cell: ColorCellButtons)
    func segmentedIndex(cell: ColorCellButtons)
    func segmentedIndexforRadius(cell: ColorCellButtons)
    func radiusPickerView(cell: ColorCellButtons)
    func radiusSliderValue(cell: ColorCellButtons)
    func customSlider2(cell: ColorCellButtons)
    func viewSegment(cell: ColorCellButtons)
}


class ColorCellButtons: UITableViewCell
{
    var colorDelegate: ColorCellDelegate?
    @IBOutlet weak var purpleButton: UIButton!
    @IBOutlet weak var blueButton: UIButton!
    @IBOutlet weak var redButton: UIButton!
    @IBOutlet weak var greenButton: UIButton!
    @IBOutlet weak var segment: SegmentVc!
    @IBOutlet weak var segmentLabel: UILabel!
    
    @IBOutlet weak var maxRadiusLabel: UILabel!
    @IBOutlet weak var minRadiusLabel: UILabel!
    
    
    @IBOutlet weak var radiusSegmentLabel: UILabel!
    
    @IBOutlet weak var radiusValueLabel: UILabel!
    
    @IBOutlet weak var radiusSliderLabel: UISlider!
    
    @IBOutlet weak var maxValImg: UIImageView!
    
    
    @IBOutlet weak var viewSegment: UISegmentedControl!
    
    @IBOutlet weak var viewSegmentLabel: UILabel!
    
    @IBAction func viewSegment(_ sender: Any)
    {
        if let delegate = colorDelegate
        {
            delegate.viewSegment(cell: self)
        }
    }
    
    
    @IBAction func radiusSliderValue(_ sender: Any)
    {
        var position = radiusSliderLabel.value
        print(position)
        
        if let delegate = colorDelegate
        {
            delegate.radiusSliderValue(cell: self)
        }
    }
    
    @IBOutlet weak var silderThumbLabel: UILabel!
    
    @IBOutlet weak var radiusPicker: UIPickerView!
    
    
    
    @IBAction func purpleTapped(_ sender: Any)
    {
        if let delegate = colorDelegate
        {
            delegate.purpleTapped(cell: self)
        }
    }
    
    
    @IBAction func blueTapped(_ sender: Any)
    {
        if let delegate = colorDelegate
        {
            delegate.blueTapped(cell: self)
        }
    }
    
    @IBAction func redTapped(_ sender: Any)
    {
        if let delegate = colorDelegate
        {
            delegate.redTapped(cell: self)
        }
    }
    
    @IBAction func greenTapped(_ sender: Any)
    {
        if let delegate = colorDelegate
        {
            delegate.greenTapped(cell: self)
        }
    }
    
    @IBAction func segmentedIndex(_ sender: Any)
    {
        if let delegate = colorDelegate
        {
            delegate.segmentedIndex(cell: self)
        }
    }
    
    
}
