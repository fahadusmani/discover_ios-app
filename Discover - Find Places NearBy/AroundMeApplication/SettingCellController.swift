//
//  SettingCellController.swift
//  AroundMeApplication
//
//  Created by Admin on 6/23/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class SettingCellController
{
    func getSettingsTableData(cell: UITableViewCell, row: Int) -> UITableViewCell
    {
        var newCell = cell
        newCell.selectionStyle = .none
        if (row == 0 || row == 2 || row == 4 || row == 6 || row == 8)
        {
            newCell.textLabel?.font = colorSelect.titleFont
            newCell.textLabel?.textColor = colorSelect.color
            
            switch row
            {
            case 0:
                cell.textLabel?.text = "Theme"
                break
                
            case 2:
                cell.textLabel?.text = "Units"
                break
                
            case 4:
                cell.textLabel?.text = "Radius"
                break
                
            default:
                break
            }
            return newCell
        }
            
        else
        {
            newCell.textLabel?.font = colorSelect.textFont
            newCell.backgroundColor = UIColor.white
            newCell.backgroundColor = colorSelect.settingsTitleColor
            
            switch row
            {
                
            default:
                break
            }
            return cell
        }
    }
    
    
    func applyCellAttributes(cell: ColorCellButtons, index: Int) -> ColorCellButtons
    {
        if(index == 1)
        {
            cell.backgroundColor = colorSelect.settingsTitleColor
            cell.purpleButton.setImage(#imageLiteral(resourceName: "setting_purple"), for: .normal)
            cell.blueButton.setImage(#imageLiteral(resourceName: "setting_blue"), for: .normal)
            cell.redButton.setImage(#imageLiteral(resourceName: "setting_red"), for: .normal)
            cell.greenButton.setImage(#imageLiteral(resourceName: "setting_black"), for: .normal)
            
            switch SettingVc.selected {
                
            case 1 :
                cell.purpleButton.setImage(#imageLiteral(resourceName: "setting_purpleTick"), for: .normal)
            case 2:
                cell.blueButton.setImage(#imageLiteral(resourceName: "setting_blueTick"), for: .normal)
            case 3:
                cell.redButton.setImage(#imageLiteral(resourceName: "setting_redTick"), for: .normal)
            case 4:
                cell.greenButton.setImage(#imageLiteral(resourceName: "setting_blackTick"), for: .normal)
            default:
                break
            }
            
            cell.selectionStyle = .none
            
            return cell
        }
            
        else //if index == 3
        {
            cell.segmentLabel.font = colorSelect.textFont
            cell.backgroundColor = colorSelect.settingsTitleColor
            cell.segment.tintColor = colorSelect.color
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    
    
}
