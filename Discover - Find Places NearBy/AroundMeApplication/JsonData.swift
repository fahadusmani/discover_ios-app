//
//  JsonData.swift
//  AroundMeApplication
//
//  Created by Admin on 4/20/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import GoogleMaps

class JsonData
{
    var mapObj = LoadMap()
    var open_hours : [String]?
    var name = [Info]()
    var pic = [Photo]()
    var geometry = [Geometry]()
    var placeDataa = [ListDataArray]()
    static var nextPageToken = ""
    static var updateDistanceValue = 0
    
    func readJSONObject(object: [String: Any] ) -> [Info]
    {
        var data = Info()
        var data1 = Photo()
        var data2 = Geometry()
        
        if(JsonData.updateDistanceValue == 1)
        {
            name.removeAll()
        }
        JsonData.updateDistanceValue = 0
        
        if((object["next_page_token"]) != nil)
        {
            JsonData.nextPageToken = object["next_page_token"] as! String
        }
        else
        {
            JsonData.nextPageToken = ""
        }
        
        var obj = object["results"] as! [(AnyObject)]
        
        for i in obj
        {
            data.name = i["name"] as? String
            data.id = i["place_id"] as? String
            data.location = i["vicinity"] as? String
            data.rating = i["rating"] as? Double
            
            let chkPhotos = i["photos"]
            let chkGeo = i["geometry"]
            let a = chkPhotos
            let openNow = i["opening_hours"]
            
            if chkPhotos! == nil
            {
                data1.photoname = nil
                data1.height = nil
                data1.width = nil
            }
            else
            {
                if let obj1 = i["photos"] as? [(AnyObject)]
                {
                    for j in obj1
                    {
                        let photoname = j["html_attributions"] as! [String]
                        for k in photoname
                        {
                            data1.photoname = k as String
                        }
                        data1.height = j["height"] as? Double
                        data1.width = j["width"] as? Double
                    }
                }
            }
            
            
            if(openNow == nil)
            {
                data.open = true
            }
            else
            {
                if let obj2 = i["opening_hours"] as (AnyObject)!
                {
                    if obj2["open_now"] != nil
                    {
                        data.open = obj2["open_now"] as! Bool
                    }
                    else
                    {
                        data.open = true
                    }
                }
            }
            
            
            if (chkGeo != nil)
            {
                if let obj2 = i["geometry"] as ((AnyObject))!
                {
                    let locationObj = obj2["location"] as (AnyObject)!
                    
                    
                    data2.location_latitude = locationObj?["lat"] as! Double
                    data2.location_longitude = locationObj?["lng"] as! Double
                    
                    let viewPortObj = obj2["viewport"] as (AnyObject)!
                    
                    let northEastObj = viewPortObj?["northeast"] as (AnyObject)!
                    data2.viewport_northeast_latitude = northEastObj?["lat"] as! Double
                    data2.viewport_northeast_longitude = northEastObj?["lng"] as! Double
                    
                    let southWestObj = viewPortObj?["southwest"] as (AnyObject)!
                    data2.viewport_southwest_latitude = southWestObj?["lat"] as! Double
                    data2.viewport_southwest_longitude = southWestObj?["lng"] as! Double
                }
            }
            data.placeDistance = mapObj.calculateDistance(lat: data2.location_latitude!, long: data2.location_longitude!)
            
            
            self.geometry = [data2]
            self.pic = [data1]
            data.geometryArray = self.geometry
            data.photoArray = self.pic
            self.name.append(data)
        }
        
        if(name.count > 1)
        {
            for x in 0...name.count-1
            {
                for y in 0...x
                {
                    if(name[y].placeDistance! > name[x].placeDistance!)
                    {
                        let smallValue = name[y]
                        name[y] = name[x]
                        name[x] = smallValue
                    }
                }
            }
        }
        return name
    }
    
    func readJSONObject1(object: [String: Any] ) ->  [ListDataArray]
    {
        var dataa = ListDataArray()
        let obj1 = object["result"] as (AnyObject)
        if let openHoursString = obj1["opening_hours"] as (AnyObject)?
        {
            if openHoursString == nil
            {
                open_hours = []
                dataa.shedule = []
            }
            else
            {
                if let hrs = openHoursString["weekday_text"]
                {
                    open_hours = openHoursString["weekday_text"] as! [String]
                    dataa.shedule = openHoursString["weekday_text"] as! [String]
                }
                else{
                    open_hours = []
                    dataa.shedule = []
                }
            }
            
            let placeWebsite : String?
            let placePhone : String?
            let chk = obj1["website"]!
            let chk1 = obj1["formatted_phone_number"]!
            if(chk != nil)
            {
                placeWebsite = obj1["website"] as!
                String
            }
            else
            {
                placeWebsite = ""
            }
            if(chk1 != nil)
            {
                placePhone = obj1["formatted_phone_number"] as! String
            }
            else
            {
                placePhone = ""
            }
            dataa.website = placeWebsite
            dataa.phone = placePhone
        }
        placeDataa = [dataa]
        return placeDataa
    }
    
}
