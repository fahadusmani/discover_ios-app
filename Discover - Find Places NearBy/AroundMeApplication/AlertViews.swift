//
//  AlertViews.swift
//  AroundMeApplication
//
//  Created by Admin on 5/17/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class AlertViewws
{
    var appMapObj = AppleMap()
    
    func setMapAlert(placeName : String, latitude : Double, Longitude : Double) -> UIAlertController
    {
        var alert = UIAlertController(title: "Select MapView",
                                      message: "",
                                      preferredStyle: .alert)
        
        let googleMapsAction = UIAlertAction(title: "Google Maps", style: .default, handler: { (action) -> Void in
            
            if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
                UIApplication.shared.openURL(NSURL(string:
                    "comgooglemaps://?saddr=\(LoadMap.latitude!),\(LoadMap.longitude!)&daddr=\(latitude),\(Longitude)&directionsmode=driving")! as URL)
                
            } else
            {
                // NSLog("Can't use comgooglemaps://");
                print("google maps not found")
                
            }
            
        })
        let appleMapsAction = UIAlertAction(title: "Apple Maps", style: .default, handler: { (action) -> Void in
            self.appMapObj.openMapForPlace(placeName: placeName, lat: latitude, long: Longitude)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(googleMapsAction)
        alert.addAction(appleMapsAction)
        alert.addAction(cancel)
        return alert
    }
    
    func setInternetAlert() -> UIAlertController
    {
        let alertController = UIAlertController (title: "No Wifi or cellular data is connected", message: "It is found that your phone's wifi or cellular data connection are turned off. Please make sure that you have connected to an active internet connection.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: "App-Prefs:root=WIFI") else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
    func setLocationAlert() -> UIAlertController
    {
        let alertController = UIAlertController(title: NSLocalizedString("Unable to use location services", comment: ""), message: NSLocalizedString("It is found that your location services for this application are turned off. Please make sure that you allow location services for this application.", comment: ""), preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel, handler: nil)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default) { (UIAlertAction) in
            UIApplication.shared.openURL(NSURL(string: UIApplicationOpenSettingsURLString)! as URL)
            
            exit(0)
        }
        alertController.addAction(cancelAction)
        alertController.addAction(settingsAction)
        return alertController
    }
    
    func setPlacesAlert(key: String) -> UIAlertController
    {
        let alert = UIAlertController(title: "No \(key)s are found in seleced radius", message: "",                                      preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(ok)
        return alert
    }
    
    
    func setShareWidget(array: [DetailDataArray], array1: [ListDataArray]) -> UIActivityViewController
    {
        let name = "Name : \(array[0].name!) \n"
        let address = "Address : \(array[0].location!) \n"
        let link = (URL(string:"https://www.google.com/maps/@\(array[0].latitude!),\(array[0].longitude!),18z")!)
        
        var web : String?
        var phonee : String?
        
        if(array1[0].website != nil)
        {
            web = "\nWebsite : \(NSURL(string: array1[0].website!)!) \n"
        }
        else
        {
            web = ""
        }
        
        if(array1[0].phone != nil)
        {
            phonee = "Phone : \(array1[0].phone!) \n"
        }
        else
        {
            phonee = ""
        }
        
        var activityViewController = UIActivityViewController(activityItems: [name as! NSString, address as NSString, link, web!, phonee! as NSString], applicationActivities: nil)
        
        return activityViewController
    }
}
