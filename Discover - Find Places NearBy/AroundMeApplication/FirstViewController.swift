                       //
                       //  FirstViewController.swift
                       //  AroundMeApplication
                       //
                       //  Created by Admin on 5/25/17.
                       //  Copyright © 2017 Mujadidia. All rights reserved.
                       //
                       
                       import UIKit
                       import BubbleTransition
                       
                       class FirstViewController: UIViewController, UIViewControllerTransitioningDelegate {
                        
                        let transition = BubbleTransition()
                        
                        @IBOutlet weak var logoImageLabel: UIImageView!
                        
                        override func viewDidLoad() {
                            super.viewDidLoad()
                            
                            logoImageLabel.image = #imageLiteral(resourceName: "logoScreen")
                            var timer = Timer.scheduledTimer(timeInterval: 0.4, target: self, selector: #selector(self.update), userInfo: nil, repeats: false);
                        }
                        
                        override var prefersStatusBarHidden: Bool {
                            return false
                        }
                        
                        override func didReceiveMemoryWarning() {
                            super.didReceiveMemoryWarning()
                        }
                        
                        
                        var statusBarView: UIView? {
                            return value(forKey: "statusBar") as? UIView
                        }
                        
                        override open var preferredStatusBarStyle: UIStatusBarStyle {
                            return .lightContent
                        }

                        
                        
                        func update() {
                            performSegue(withIdentifier: "toHomeScreen", sender: self)
                        }
                        
                        
                        public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
                            let controller = segue.destination
                            controller.transitioningDelegate = self
                            controller.modalPresentationStyle = .custom
                            
                        }
                        
                        
                        public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
                            transition.duration = 0.6
                            transition.transitionMode = .present
                            transition.startingPoint = self.logoImageLabel.center
                            transition.bubbleColor = UIColor.white
                            return transition
                        }
                        
                        public func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
                            transition.transitionMode = .dismiss
                            transition.startingPoint = self.logoImageLabel.center
                            transition.bubbleColor = UIColor.blue
                            
                            return transition
                        }

                        
                        
                        
                        
                       }
                       
                       
                       
