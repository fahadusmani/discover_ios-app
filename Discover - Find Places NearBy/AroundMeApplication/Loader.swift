//
//  defaultLoader.swift
//  AroundMeApplication
//
//  Created by Admin on 5/16/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView


class Loader
{
    func showActivityIndicatory(uiView: UIView) -> UIActivityIndicatorView
    {
        var actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect.init(x: 0.0, y: 0.0, width: 40.0, height: 40.0)
        actInd.center = uiView.center
        actInd.hidesWhenStopped = true
        actInd.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        actInd.color = colorSelect.color
        // actInd.backgroundColor = UIColor.lightGray
        uiView.addSubview(actInd)
        
        return actInd
    }
    
    func showActivityIndicatory1(uiView: UIView) -> NVActivityIndicatorView
    {
        var actInd: NVActivityIndicatorView
        var frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        
        actInd = NVActivityIndicatorView(frame: frame, type: .ballPulse, color: colorSelect.color, padding: nil)
        
        actInd.center = uiView.center
        uiView.addSubview(actInd)
        actInd.tag = 100
        
        return actInd
    }
    
    
}
