//
//  ListVcCellController.swift
//  AroundMeApplication
//
//  Created by Admin on 6/23/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import UIKit

class ListVcCellConroller
{
    var imageObject = ImageColor()
    var menu_Data_Obj = Menu_Data()
    var mapObj = LoadMap()
    
    func applyCellAttributes(cell: ListTableCell, index: Int, data: [menuData], jsonData: [Info]) -> ListTableCell
    {
        let img = UIImage(named: data[0].icon!)
        let coloredImage = imageObject.maskWithColor(img: img!, color: colorSelect.color!)
        let coloredDistanceImage = imageObject.maskWithColor(img: #imageLiteral(resourceName: "miles"), color: colorSelect.color!)
        
        cell.distanceLabel.font = UIFont(name: "Noto Sans", size: 13.0)
        if(ListVc.distanceUnitKm == true)
        {
            cell.distanceLabel.text = "\(jsonData[index].placeDistance!) km"
        }
        else
        {
            cell.distanceLabel.text = "\(jsonData[index].placeDistance!) miles"
        }
        cell.dataLabel?.font = UIFont(name: "Noto Sans", size: 15.0)
        cell.dataLabel?.textColor = colorSelect.color
        cell.dataLabel.text = "\(jsonData[index].name!)"
        cell.cellDetailData.font = UIFont(name: "Noto Sans", size: 12.0)
        cell.cellDetailData?.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.cellDetailData?.numberOfLines = 0
        cell.cellDetailData.text = "\(jsonData[index].location!)"
        cell.cellImage.image = coloredImage
        cell.distanceImage.image = coloredDistanceImage
        cell.selectionStyle = .none
        
        return cell
        
    }
    
    func setDataToSend(row: Int, array: [Info]) -> [DetailDataArray]
    {
        var x = 0
        var getData = DetailDataArray()
        getData.name = array[row].name!
        getData.location = array[row].location!
        getData.latitude = array[row].geometryArray?[0].location_latitude!
        getData.longitude = array[row].geometryArray?[0].location_longitude!
        getData.openNow = array[row].open
        getData.placeId = array[row].id
        getData.distance = mapObj.calculateDistance(lat: (array[row].geometryArray?[0].location_latitude!)!, long: (array[row].geometryArray?[0].location_longitude!)!)
        getData.count = x
        let dataArray = getData
        
        return [dataArray]
        
    }
    
}
