//
//  InfoVc.swift
//  AroundMeApplication
//
//  Created by Admin on 4/18/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import GoogleMobileAds

class InfoVc: UIViewController, UITableViewDataSource, UITableViewDelegate, GADBannerViewDelegate
{
    @IBOutlet weak var infoTable: UITableView!
    @IBOutlet weak var infoAdsView: GADBannerView!
    
    var googleAds = GoogleAdsServices()
    var callingServiceObject = CallingService()
    var objColor = colorSelect()
    var imageObj = ImageColor()
    var info_Data_Obj = Info_Data()
    var infoArray : [infoData]?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return (infoArray?.count)!
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cellIdentifier = "cell"
        
        let icon = infoArray![indexPath.row].icon
        let img = imageObj.maskWithColor(img: UIImage(named: icon!)!
            , color: colorSelect.color!)
        
        var cell : UITableViewCell  = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)! as UITableViewCell
        cell.textLabel!.text = infoArray![indexPath.row].namee
        
        cell.imageView?.image = img
        cell.textLabel?.font = colorSelect.titleFont
        
        let cellAccessoryImage = imageObj.maskWithColor(img: #imageLiteral(resourceName: "Info_DiscloserIndicator1"), color: colorSelect.color!)
        
        cell.accessoryType = .disclosureIndicator
        cell.accessoryView = UIImageView(image: cellAccessoryImage)
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if indexPath.row == 0
        {
            performSegue(withIdentifier: "toAboutVc", sender: self)
        }
        
        if indexPath.row == 1
        {
            if let url = URL(string: "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=1245055646"),
                UIApplication.shared.canOpenURL(url){
                UIApplication.shared.openURL(url)
            }
        }
        
        if indexPath.row == 2
        {
            let text = "Discover-Find Places Nearby is a user-friendly app that helps you to search nearby places and provide all available details for such places. The app uses background location services but we will not share your location data with any other third party. \n\nDownload Discover App from AppStore here:\n itunes.apple.com/pk/app/discover-find-places-nearby/id1245055646?mt=8"
            
            var activityViewController = UIActivityViewController(activityItems: [text], applicationActivities: nil)
            present(activityViewController, animated: true, completion: {})
        }
        
        if indexPath.row == 3
        {
            callingServiceObject.openWebsite(website: "itms-apps://itunes.apple.com/pk/developer/mujadida-inc/id1240213107")
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        objColor.setNavigationTitleImage(item: self.navigationItem)
        googleAds.loadAds(myBanner: infoAdsView)
        infoAdsView.rootViewController = self
        infoArray = info_Data_Obj.getData()
        infoTable.tableFooterView = UIView(frame: .zero)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.tabBarController?.tabBar.isHidden = false
        objColor.fetchColor()
        objColor.applyColor(nav: navigationController!, bar: tabBarController!)
        infoTable.reloadData()
    }
}
