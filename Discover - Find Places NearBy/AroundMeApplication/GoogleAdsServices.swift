//
//  GoogleAdsServices.swift
//  AroundMeApplication
//
//  Created by Admin on 6/22/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation
import GoogleMobileAds

class GoogleAdsServices : GADBannerView, GADBannerViewDelegate
{
    
    func loadAds(myBanner : GADBannerView)
    {
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        myBanner.adUnitID = "ca-app-pub-8996381549924945/7512749712"
        
        myBanner.delegate = self
        myBanner.load(request)
    }
    
}
