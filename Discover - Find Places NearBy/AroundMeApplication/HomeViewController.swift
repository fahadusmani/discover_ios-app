//
//  HomeViewController.swift
//  AroundMeApplication
//
//  Created by Admin on 6/19/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import UIKit
import GoogleMobileAds

class HomeViewController: UIViewController {
    
    var googleAds = GoogleAdsServices()
    var applyTheme = colorSelect()
    
    @IBOutlet weak var myBanner: GADBannerView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        googleAds.loadAds(myBanner: myBanner)   //loading ADS
        myBanner.rootViewController = self
        applyTheme.setNavigationTitleImage(item: self.navigationItem) //Setting Navigation Image
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
