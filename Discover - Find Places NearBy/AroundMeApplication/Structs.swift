//
//  Structs.swift
//  AroundMeApplication
//
//  Created by Admin on 4/24/17.
//  Copyright © 2017 Mujadidia. All rights reserved.
//

import Foundation

public struct menuData
{
    var namee : String?
    var icon : String?
    var selectedKey : String?
}

public struct ListDataArray
{
    var website : String?
    var phone : String?
    var shedule : [String]?
}

public struct placeData {
    var website : String?
    var phone : String?
}


public struct infoData {
    var namee : String?
    var icon : String?
}

public struct DetailDataArray
{
    var name : String?
    var location : String?
    var latitude : Double?
    var longitude : Double?
    var website : String?
    var phone : String?
    var count : Int?
    var openNow : Bool?
    var placeId : String?
    var distance : Double?
    var timings : [String]?
}


public struct Info
{
    var name : String?
    var location : String?
    var rating : Double?
    var id : String?
    // var photoname : String?
    //  var width : Double?
    var photoArray : [Photo]?
    var geometryArray : [Geometry]?
    var website : String?
    var phone : String?
    var open : Bool?
    var placeDistance : Double?
}


public struct Photo
{
    var photoname : String?
    var height : Double?
    var width : Double?
}


public struct Geometry
{
    var location_latitude : Double?
    var location_longitude : Double?
    var viewport_northeast_latitude : Double?
    var viewport_northeast_longitude : Double?
    var viewport_southwest_latitude : Double?
    var viewport_southwest_longitude : Double?
}
